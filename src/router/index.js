import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import AboutComponente  from '../components/AboutComponente.vue'
import GatoComponent  from '../components/GatoComponent.vue'
import PerroComponent  from '../components/PerroComponent.vue'
import LoginRegistroComponent  from '../components/LoginRegistroComponent.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/perro',
    name: 'perro',
    component: PerroComponent
  },
  {
    path: '/about',
    name: 'About',
    component: AboutComponente
  },
  {
    path: '/login',
    name: 'Login',
    component: LoginRegistroComponent
  },
  {
    path: '/gatos',
    name: 'Gatos',
    component: GatoComponent
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
